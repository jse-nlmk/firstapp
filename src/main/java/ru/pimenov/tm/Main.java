package ru.pimenov.tm;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int i = 1;
        do {
            Scanner keyboard = new Scanner(System.in);
            System.out.println("\nenter an integer (0 for exit)");
            i = keyboard.nextInt();
            System.out.printf("Введено число:%d\n", i);
        } while (i != 0);
        System.out.println("Выход");
        System.exit(0);
    }

    public int plus(int a, int b){
        return a+b;
    }
}
