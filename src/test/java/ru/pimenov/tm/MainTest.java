package ru.pimenov.tm;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void testMain() {
        final int expectedValue = 3;
        Main m = new Main();
        Assert.assertEquals(expectedValue, m.plus(1, 2));
    }
}